from svm import *
from multiprocessing import Process, Value, Array, Queue, Lock
import numpy  as np
import pandas as pd
import random
import time

# Train Variables
train_features = None
train_labels   = None
w              = None
token_w        = None

# SVM Variables
finished_train_samples = None
counter = None

epoch         = None
cost          = None
got_token     = None
done_cores    = None
done_cores_locks = None
done_epoch    = None
done_training = None

# Debug Variables
debug = False
print_data_samples = False

class Core():
    def __init__(self, _train_features, _train_labels, h_vars, args_dict, queue_dict):
        global train_features, train_labels, counter, w, token_w, finished_train_samples, epoch, got_token, done_epoch, done_training
        global done_cores, done_cores_locks, train_time, end_time, cost
        # Set globals
        train_features = _train_features
        train_labels   = _train_labels
        


        self.cluster_id = args_dict['cluster_id']          # Cluster ID
        self.core_id    = args_dict['core_id']             # Core ID
        self.p          = Process(target=self.core_driver) # Process to start SGD on this core

        # Hogwild++ Variables
        cost = Value('d', 0)
        train_time = Value('d', 0)
        self.end_time = -1
        self.start_time = -1
        self.h_vars        = h_vars                        # hogwild_vars from main
        self.use_token     = (h_vars['cluster_count'] > 1)
        self.feature_count = args_dict['feature_count'] # Number of features
        self.total_train_samples = args_dict['total_train_samples']
        
        # Counter representing amt of examples until passing token
        # If -1 * core_count - 1, do not have token. If > 0, do have token
        first_counter    = [self.h_vars['examples_until_pass_token']]
        rest_of_counters = [-1 * self.h_vars['cluster_count'] - 1 for i in range(self.h_vars['cluster_count'] - 1)]

        counter = Array('i', first_counter + rest_of_counters)
        w       = Array('d', [0]*self.h_vars['cluster_count']*self.feature_count)
        token_w = Array('d', [0]*self.h_vars['cluster_count']*self.feature_count)

        if self.core_id == 0 and self.cluster_id == 0:
            finished_train_samples = Value('i', 0)
            epoch         = Value('d', 0) # Epoch count
            got_token     = Value('i', False)
            done_epoch    = Value('i', False)
            done_training = Value('i', False)
            done_cores    = Array('i', [0]*self.h_vars['cluster_count'])
            done_cores_locks = [None for i in range(self.h_vars['cluster_count'])]

        # Inter-cluster Variables
        # - token_queue - pass tokens
        # - w_queue - pass w to next cluster, and receive from previous cluster
        # - request_queue - request w from next cluster
        # - core-to-main and vice versa - queues to communicate to main driver
        self.upstream_token_queue     = queue_dict['upstream_token_queue']
        self.downstream_token_queue   = queue_dict['downstream_token_queue']
        self.upstream_w_queue         = queue_dict['upstream_w_queue']
        self.downstream_w_queue       = queue_dict['downstream_w_queue']
        self.upstream_request_queue   = queue_dict['upstream_request_queue']
        self.downstream_request_queue = queue_dict['downstream_request_queue']   
        self.main_to_core_queue    = queue_dict['main_to_core_queue']
        self.core_to_main_queue    = queue_dict['core_to_main_queue']

        # Sync lock
        self.sync_lock = args_dict['sync_lock']
        self.done_cores_lock = args_dict['done_cores_lock']


    # Start SGD on core
    def start(self):
        self.p.start()


    # Stop SGD on core
    def join(self):
        self.p.terminate()

    def get_current_sample(self):

        return finished_train_samples.value * (epoch.value + 1)

    def get_train_time(self):
        global train_time
        train_time.value += self.end_time - self.start_time
        return train_time.value

    def get_cost(self):
        return cost.value

    # Get + set w for a certain id
    def w_get(self):
        return w[ self.feature_count*self.cluster_id:self.feature_count*(self.cluster_id + 1) ]


    def w_set(self, new_w):
        global w
        w[ self.feature_count*self.cluster_id:self.feature_count*(self.cluster_id + 1) ] = new_w


    # Get + set token_w for a certain id
    def token_w_get(self):
        return token_w[ self.feature_count*self.cluster_id:self.feature_count*(self.cluster_id + 1) ]


    def token_w_set(self, new_token_w):
        global token_w
        token_w[ self.feature_count*self.cluster_id:self.feature_count*(self.cluster_id + 1) ] = new_token_w

    # Check queues. Three scenarios:
    # 1. Previous cluster sends w. Update our w.
    # 2. Previous cluster requests w. Pass w backwards
    # 3. Previous cluster sends token. Synchronize!
    def check_queues(self):
        global counter
        # 1. Previous cluster sends w. Update our w.
        if (self.downstream_w_queue.empty() == False and self.h_vars['use_downstream_updates'] == True):
            msg = self.downstream_w_queue.get()
            if debug: print("Cluster " + str(self.cluster_id) + ", core " + str(self.core_id) + ": Updated w sent from previous cluster")
            self.w_set(msg)
            
        # 2. Previous cluster requests w. Pass w backwards
        if (self.downstream_request_queue.empty() == False):
            msg = self.downstream_request_queue.get()
            if debug: print("Cluster " + str(self.cluster_id) + ", core " + str(self.core_id) + ": Received message " + str(msg) + " from downstream_request_queue")
            self.downstream_w_queue.put(self.w_get())
            if debug: print("Cluster " + str(self.cluster_id) + ", core " + str(self.core_id) + ": Sent w to previous cluster")

        # 3. Previous cluster sends token. Synchronize!
        # - a) Request w from next cluster
        # - b) Wait for w from next cluster
        # - c) Synchronize() (calculate updates to next cluster's w)
        # - d) Send w to next cluster
        # - e) Hold token for <examples_until_pass_token> training examples
        if (self.downstream_token_queue.empty() == False and self.use_token == True):
            # Receive token
            if debug: print("Cluster " + str(self.cluster_id) + ", core " + str(self.core_id) + ": Got token.")
            got_token.value = self.downstream_token_queue.get()

            # a) Request w from next cluster
            self.upstream_request_queue.put("gimme w")
            if debug: print("Cluster " + str(self.cluster_id) + ", core " + str(self.core_id) + ": Request for w sent to next cluster!")

            # b) Wait for w from next cluster
            if debug: print("Cluster " + str(self.cluster_id) + ", core " + str(self.core_id) + ": Waiting for w from next cluster...")
            next_w = self.upstream_w_queue.get()
            if debug: print("Cluster " + str(self.cluster_id) + ", core " + str(self.core_id) + ": Received next cluster's w")
            
            # c) Synchronize() (calculate updates to next cluster's w)
            next_w_updated = self.synchronize(epoch.value, next_w)

            # d) Send w to next cluster
            self.upstream_w_queue.put(next_w_updated)
            if debug: print("Cluster " + str(self.cluster_id) + ", core " + str(self.core_id) + ": Sent updated w to next cluster")

            # e) Hold token for <examples_until_pass_token> training examples
            counter[self.cluster_id] = self.h_vars['examples_until_pass_token']
            got_token.value = False

    # synchronize() - synchronizes our local vectors using previous and next clusters
    # - Normal local vector = w that each cluster updates with every example
    # - Token local vector  = w that each cluster updates everytime it gets the token
    # 1. Update this cluster's token local vector
    # 2. Update next cluster's local vector
    # 3. Update this cluster's local vector
    def synchronize(self, epoch, _next_w):
        w        = np.array(self.w_get())  # Normal local vector
        token_w  = np.array(self.token_w_get())  # Token local vector
        next_w   = np.array(_next_w)       # Normal local vector from next cluster

        # 1. Update this cluster's token local vector
        dW = w - token_w
        beta = 0.8          ### Should be the root of beta^m + beta = 1 ###
        step_decay = self.h_vars['initial_step_decay']**epoch
        lamda      = 1 - (beta)**(self.h_vars['cluster_count'] - 1)
        self.token_w_set(list( (lamda)*next_w + (1-lamda)*token_w + (beta * step_decay) * dW))

        # 2. Update next cluster's local vector
        next_w_updated = list(next_w + (beta * step_decay) * dW)

        # 3. Update this cluster's local vector
        self.w_set(self.token_w_get())

        return next_w_updated

    # If we get token, the first core to read the token will synchronize!
    # If we get requests from downstream, first core to read will service request
    # Otherwise, train!
    def core_driver(self):
        global done_epoch, epoch, finished_train_samples, done_training, done_cores
        # Keep core doing SGD while training
        check_queue_counter = 0
        
        while done_training.value == False:

            ####################################
            # Wait for the go signal from main #
            ####################################
            #start_time = time.time()
            if debug: print("Cluster " + str(self.cluster_id) + ", core " + str(self.core_id) + ": Waiting for go from main_to_cores.")
            while self.main_to_core_queue.empty():
                if self.sync_lock.acquire(False):
                    self.check_queues()
                    self.sync_lock.release()
            self.main_to_core_queue.get()
            if debug: print("Cluster " + str(self.cluster_id) + ", core " + str(self.core_id) + ": Got go from main_to_cluster.")
           # print("Time to check go signal from main: " + str(time.time() - start_time))
            #############
            # Run epoch #
            #############
            # 1. Service any requests to queues. Only 1 core can do this at a time.
            # 2. Pass token if it is time.       Only 1 core can do this at a time.
            # 3. Perform SGD on one sample.
            # 4. Check if done with epoch.
            self.start_time = time.time()
            while done_epoch.value == False:
                # 1. Service any requests to queues. Only 1 core can do this at a time.
                if self.use_token:
                    if check_queue_counter >= self.h_vars['check_queue_frequency']:
                        if self.sync_lock.acquire(False):
                            self.check_queues()
                            check_queue_counter = 0
                            self.sync_lock.release()
                    else:
                        check_queue_counter += 1

                    # 2. Pass token if it is time. Only 1 core can do this at a time.
                    if counter[self.cluster_id] <= 0 and counter[self.cluster_id] != (-1*self.h_vars['cores_per_cluster'] - 1):
                        if debug: print("Cluster " + str(self.cluster_id) + ", core " + str(self.core_id) + ": Passing token to next cluster")
                        counter[self.cluster_id] = -1*self.h_vars['cores_per_cluster'] - 1
                        self.upstream_token_queue.put(True)

                # 3. Perform SGD on one sample.
                if finished_train_samples.value < self.total_train_samples:
                    self.sgd_one_sample()

                # 4. Check if done with epoch.
                if finished_train_samples.value >= self.total_train_samples - 1:
                    #print("Finished epoch! finished_Train_samples: " + str(finished_train_samples.value))
                    done_epoch.value = True

            # Reset training variables
            self.done_cores_lock.acquire()
            done_cores[self.cluster_id] += 1
            
            if done_cores[self.cluster_id] >= self.h_vars['cores_per_cluster']:
                if self.cluster_id == 0:    # Should only be updated once
                    train_time.value += time.time() - self.start_time
                    #print("Time to finish 1 epoch: " + str(train_time.value))
                    epoch.value += 1
                finished_train_samples.value = 0
                done_epoch.value = False
                done_cores[self.cluster_id] = 0
                self.core_to_main_queue.put("done!")
                if epoch.value > self.h_vars['total_epochs']:
                    done_training.value = True
                    
            self.done_cores_lock.release()

    # Perform SGD on one sample
    def sgd_one_sample(self):
        global counter, finished_train_samples, train_time
        # If we approach the end of epoch, limit cluster to only 1 core
        # limit = (finished_train_samples.value > self.total_train_samples - self.h_vars['cluster_count'])
        # if limit and self.core_id != 0:
        #     return
        
        # Get random example
        ind = random.randrange(0, self.total_train_samples)  

        # Calculate dw
        curr_w = self.w_get()
        dw = calculate_cost_gradient(self.h_vars, curr_w, train_features[ind], train_labels[ind])

        # Update weights with dw
        new_w = curr_w - (self.h_vars['learning_rate'] * self.h_vars['initial_step_decay']**epoch.value * dw)
        #print("new_w: " + str(new_w))
        self.w_set(new_w[0])
        #print(new_w[0])

        # Increment finished_train_samples
        finished_train_samples.value += 1

        # Update counter if waiting to pass token
        if counter[self.cluster_id] > 0:
            counter[self.cluster_id] -= 1

        if print_data_samples: print("Cluster " + str(self.cluster_id) + ", core " + str(self.core_id) + ": Finished data point " + str(finished_train_samples.value) + "/" + str(self.total_train_samples))
