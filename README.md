Datasets   : covtype - 500000 train, 81012 test, 54 features       
           : news20  - 15000 train, 4996 test, 1355191 features    
           : epsilon - 400000 train, 100000 test, 2000 features    
           : rcv1    - 20242 train, 677399 test, 47236 features    
           : webspam - 300000 train, 50000 test, 250 features 

Recorded parameters:

covtype: (~70% accuracy)
    'learning_rate'           : .0000012,
    'regularization_strength' : 1000,
    'initial_step_size'       : 0.5,
    'initial_step_decay'      : 0.85

news20: (>70% accuracy)
    'learning_rate'           : .0000012,
    'regularization_strength' : 1000,
    'initial_step_size'       : 0.5,
    'initial_step_decay'      : 0.85

rcv1: (~70% accuracy?)
    'learning_rate'           : .000001,
    'regularization_strength' : 50000,
    'initial_step_size'       : 0.5,
    'initial_step_decay'      : 0.85

webspam: (>91% accuracy)
    'learning_rate'           : .0001,
    'regularization_strength' : 50,
    'initial_step_size'       : 0.5,
    'initial_step_decay'      : 0.85

a1a: (~84% accuracy)
    'learning_rate'           : .0000005,
    'regularization_strength' : 10000,
    'initial_step_size'       : 0.5,
    'initial_step_decay'      : 0.75

german: (~81% accuracy)
    'learning_rate'           : .000005,
    'regularization_strength' : 1000,
    'initial_step_size'       : 0.5,
    'initial_step_decay'      : 0.85