import numpy as np
import pandas as pd
from scipy.sparse import csr_matrix

# Return the training and testing features + labels from file
def prepare_data(v):
    print("Reading training data...")
    train_features_df = pd.read_csv(v['train_features_csv_path'])
    train_labels_df   = pd.read_csv(v['train_labels_csv_path'])

    print("Reading testing data...")
    test_features_df = pd.read_csv(v['test_features_csv_path'])
    test_labels_df   = pd.read_csv(v['test_labels_csv_path'])
    feature_count  = max(max(train_features_df['index']) + 1, max(test_features_df['index']) + 1)
    
    print("Constructing matrices... ")
    train_features = get_dataframe(train_features_df, v['total_train_samples'], 'feature', feature_count - 1)
    train_labels   = get_dataframe(train_labels_df, v['total_train_samples'],  'label')
    print("Constructed training data " + str(train_features.shape))
    test_features  = get_dataframe(test_features_df, v['total_test_samples'], 'feature', feature_count - 1)
    test_labels    = get_dataframe(test_labels_df, v['total_test_samples'], 'label')
    print("Constructed testing data  " + str(test_features.shape))

    return train_features, train_labels, test_features, test_labels, feature_count

# get_dataframe(data_threads) - reads data using <data_threads> amount of threads
def get_dataframe(data, total_samples, file_type, feature_count = -1):
    # If features, put into a csr_matrix
    if file_type == 'feature':
        row_count = data['row_count']

        # If -1, read all samples!
        if (total_samples == -1):
            total_samples = row_count.iloc[-1] + 1
            row_count = np.array(data['row_count'])
            index     = np.array(data['index'])
            value     = np.array(data['value'])

        # Otherwise, read <total_points> number of samples
        else:
            print("Total samples: " + str(total_samples))
            print(str(data['row_count'].iloc[-1]) + " < " + str(total_samples))
            if data['row_count'].iloc[-1] < total_samples:
                print("Error: Specified sample count greater than available in file.")
                exit(1)
            last_loc  = data[data['row_count'] == total_samples].index.values[0]
            row_count = np.array(data['row_count'])[:last_loc]
            index     = np.array(data['index'])[:last_loc]
            value     = np.array(data['value'])[:last_loc]

        # Add intercept
        extended_row_count = np.array([i for i in range(total_samples)])
        extended_index     = np.array([feature_count for i in range(total_samples)])
        extended_value     = np.array([1 for i in range(total_samples)])

        row_count = np.concatenate((row_count, extended_row_count))
        index     = np.concatenate((index, extended_index))
        value     = np.concatenate((value, extended_value))

        # Create csr_matrix
        return csr_matrix((value, (row_count, index)), shape=(total_samples, feature_count + 1))

    # If test, turn into np array
    elif file_type == 'label':
        if (total_samples == -1):
            return np.array(data['label'])
        else:
            if data['example'].iloc[-1] < total_samples:
                print("Error: Specified sample count greater than available in file.")
                exit(1)
            return np.array(data['label'])[:total_samples]