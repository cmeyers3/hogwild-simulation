######################################################################
# Script Name: driver.py                                             #
# Author     : Charles Meyers                                        #
# Description: Runs SVM using Hogwild++.                             #
#              to speed up data acquisition.                         #  
######################################################################

# 1. Deep dive into SVM implementation, ensure correct implementation with bias
# 1a. Deep dive into training loop
# 2. Standalone accuracy verifier
# 3. Articulate precisely the problem Hogwild++ solves
# 4. What improvements to increase concurrency + efficiency to Hogwild++?
#   - No need to sync clusters - can update at the same time!
# 5. Why is Hogwild++ Python slowing down?
# Sparsify weights
from hogwild import *
from data_collection import *
from svm import *
import numpy  as np
import pandas as pd
import random
from multiprocessing import Queue, Lock
import time
import os                           # For writing to log
from datetime import datetime       # For naming logs
import sys

debug = False

### Dataset Variables ###
if len(sys.argv) != 2:
    print("Usage: python3 driver.py <dataset>")
    exit(1)
data_set = sys.argv[1]
path_to_log_folder      = "./" + data_set + "logs/"
dataset_vars = {                                      
    'train_features_csv_path' : "./data/" + data_set + "_train_features.csv",   # Path to train_features csv file
    'train_labels_csv_path'   : "./data/" + data_set + "_train_labels.csv",     # Path to train labels csv file
    'test_features_csv_path'  : "./data/" + data_set + "_test_features.csv",    # Path to test features csv file
    'test_labels_csv_path'    : "./data/" + data_set + "_test_labels.csv",      # Path to test labels csv file
    'total_train_samples'     : -1,                                             # Total training samples (-1 if you want all)
    'total_test_samples'      : -1                                              # Total testing samples  (-1 if you want all)
}

### Training Variables ###
hogwild_vars = {
    # Hogwild++ Variables
    'cluster_count'            : None,    # Number of clusters
    'cores_per_cluster'        : None,    # Number of cores per cluster
    'examples_until_pass_token': 100,   # Number of training samples to compute before passing token
    'use_downstream_updates'   : True, # True if clusters send local model vector updates backwards
    'check_queue_frequency'    : 16,   # Times until next check_queue
    
    # SVM Variables
    'total_epochs'            : 10000,    # Total epochs to run SVM (1 epoch = entire machine runs through <total_train_samples> training samples)
    'learning_rate'           : .0001,
    'regularization_strength' : 100,
    'initial_step_size'       : 0.5,
    'initial_step_decay'      : 0.85

}

# Different datasets:
if data_set == "covtype" or data_set == "news20":
    hogwild_vars['learning_rate']           = .0000012
    hogwild_vars['regularization_strength'] = 1000
    hogwild_vars['initial_step_size']       = 0.5
    hogwild_vars['initial_step_decay']      = 0.85
if data_set == "rcv1":
    hogwild_vars['learning_rate']           = .000001
    hogwild_vars['regularization_strength'] = 50000
    hogwild_vars['initial_step_size']       = 0.5
    hogwild_vars['initial_step_decay']      = 0.85
elif data_set == "webspam":
    hogwild_vars['learning_rate']           = .0001
    hogwild_vars['regularization_strength'] = 50
    hogwild_vars['initial_step_size']       = 0.5
    hogwild_vars['initial_step_decay']      = 0.85

### Define a run of Hogwild++ here ###
print(hogwild_vars)
runs = []
cluster_count = [1]
cores_per_cluster = [1]
#cluster_count     = [1, 1, 1, 1, 1,  1,  2, 2, 2, 2, 4, 4, 4, 8, 8, 16]
#cores_per_cluster = [1, 2, 4, 8, 16, 24, 1, 2, 4, 8, 1, 2, 4, 1, 2, 1]
#cluster_count     = [1,  1,  1,  1,  1,  1,  1,  2,  2,  2,  2,  4] 
#cores_per_cluster = [24, 32, 48, 60, 72, 84, 90, 16, 24, 32, 40, 8] 
#cluster_count     = [4,  4,  8, 8, 8, 8,  16, 16, 24, 24, 32, 48, 60, 80, 90]
#cores_per_cluster = [16, 20, 2, 4, 8, 10, 2,  4,  1,  2,  1,  1,  1,  1,  1 ]

for i in range(len(cluster_count)):
    temp = hogwild_vars
    temp['cluster_count'] = cluster_count[i]
    temp['cores_per_cluster'] = cores_per_cluster[i]
    runs.append(temp.copy())

for run in runs:
    print("cluster_count    : " + str(run['cluster_count']))
    print("cores_per_cluster: " + str(run['cores_per_cluster']))

# Driver function.
def driver():
    # Prepare data
    train_features, train_labels, test_features, test_labels, feature_count = prepare_data(dataset_vars)

    # For all runs defined in h_vars:
    for i in range(len(runs)):
        print("_______Starting Run " + str(i + 1) + "/" + str(len(runs)) + "_______")

        # Initialize cores
        cores, main_to_core_queues, core_to_main_queues = init_cores(runs[i], train_features, train_labels, feature_count)
        
        # Start cores
        for j in cores: j.start()

        # Training
        cost_array, test_acc_array, sample_array, train_time = \
            train(runs[i], train_features, train_labels, test_features, test_labels, cores, main_to_core_queues, core_to_main_queues)

        # Join cores
        for j in cores: j.join()

        # Write to log
        write_to_log(runs[i], cost_array, test_acc_array, sample_array, train_time)

# Initialize clusters with proper queue connections
def init_cores(h_vars, train_features, train_labels, feature_count):
    # Create <cluster_count * cores_per_cluster> amount of cores with the proper queues.
    # Clusters are arranged in a loop, with forward and backward queues
    token_queues        = [Queue() for i in range(h_vars['cluster_count'])]
    w_queues            = [Queue() for i in range(h_vars['cluster_count'])]
    request_queues      = [Queue() for i in range(h_vars['cluster_count'])]
    main_to_core_queues = [Queue() for i in range(h_vars['cluster_count'])]
    core_to_main_queues = [Queue() for i in range(h_vars['cluster_count'])]

    # Initialize all cores with queues
    queue_array = []
    cores    = []
    for curr in range(h_vars['cluster_count']):
        prev = curr - 1
        if (prev < 0):  # Account for wrap around
            prev = h_vars['cluster_count'] - 1
        queue_dict = {
            'upstream_token_queue'     : token_queues[curr],
            'downstream_token_queue'   : token_queues[prev],
            'upstream_w_queue'         : w_queues[curr],
            'downstream_w_queue'       : w_queues[prev],
            'upstream_request_queue'   : request_queues[curr],
            'downstream_request_queue' : request_queues[prev],
            'main_to_core_queue'       : main_to_core_queues[curr],
            'core_to_main_queue'       : core_to_main_queues[curr]
        }

        queue_array.append(queue_dict)

        done_cores_lock = Lock()
        sync_lock = Lock()
        
        for j in range(h_vars['cores_per_cluster']):
            args_dict = {
                'cluster_id': curr,
                'core_id'   : j,
                'feature_count': feature_count,
                'total_train_samples': len(train_labels),
                'done_cores_lock': done_cores_lock,
                'sync_lock' : sync_lock
            }
            cores.append(Core(train_features, train_labels, h_vars, args_dict, queue_dict))

    return cores, main_to_core_queues, core_to_main_queues

# Train and return arrays to write to log
def train(h_vars, train_features, train_labels, test_features, test_labels, cores, main_to_core_queues, core_to_main_queues):

    # Logging Arrays for data
    sample_array   = [] # Sample count for each data
    cost_array     = [] # lowest cost on each epoch
    test_acc_array = [] # test_acc on each epoch

    # Start epochs
    lowest_cost, prev_lowest_cost, first_cost, change, overall_change, train_time, real_time = 0, 0, 0, 0, 0, 0, 0
    train_magnitude = 1         # Next sample == 2**train_magnitude
    data_granularity = 1        # train_magnitude += data_granularity
    max_sample_threshold = 1000 # If next sample > this, linear
    linear_magnitude = 1000     # If sample > max_sample_threshold, increment by this amount
    
    current_epoch = 0
    finished_clusters = 0
    sample_threshold = 0

    # Tell clusters to start epoch for the first time
    for j in range(h_vars['cluster_count']):
        for k in range(h_vars['cores_per_cluster']):
            main_to_core_queues[j].put("start!")

    start_training_time = time.time()
    while current_epoch < h_vars['total_epochs']:
        # Check if we need to log some sample
        current_sample = cores[0].get_current_sample()
        
        if current_sample > sample_threshold:

            # Record cost
            cost_w = cores[0].w_get()
            cost = compute_cost(h_vars, cost_w, train_features, train_labels)
            cost_array.append(cost)
            #print("Cost             :\t {}".format('%.3f'%(cost)))

            # Record accuracy
            test_acc, correct_examples, total_examples = test(test_features, test_labels, cost_w)
            test_acc_array.append(test_acc)
            sample_array.append(current_sample)
            #print("Accuracy         :\t {}% ({}/{} correct)".format('%.3f'%(test_acc_array[-1]), correct_examples, total_examples))

            # Update next sample
            train_magnitude += data_granularity
            if sample_threshold > max_sample_threshold:
                sample_threshold += linear_magnitude
            else:
                sample_threshold += 2**train_magnitude

        # Record clusters finished with epoch
        for j in range(h_vars['cluster_count']):
            if core_to_main_queues[j].empty() == False:
                core_to_main_queues[j].get()
                finished_clusters += 1

        # If all clusters finished, done with epoch!
        if finished_clusters == h_vars['cluster_count']:
            current_epoch += 1
            print("---- Epoch " + str(current_epoch) + "/" + str(h_vars['total_epochs']) + " finished.----")
            finished_clusters = 0
            for j in range(h_vars['cluster_count']):
                for k in range(h_vars['cores_per_cluster']):
                    main_to_core_queues[j].put("start!")

    # Record train_time
    train_time = cores[0].get_train_time()
    print("Final cost:         " + str(cost_array[-1]))
    print("Final test accuracy:" + str(test_acc_array[-1]) + "%")
    print("Final train time:   " + str(train_time) + " seconds.")
    return cost_array, test_acc_array, sample_array, train_time

# Retrieve test accuracy for a given <feature vector> and <test samples>
def test(test_features, test_labels, W):
    correct_examples, total_examples = 0., test_features.shape[0]

    # classification = sign( X . W )
    yp = np.sign(test_features * np.array(W).transpose())

    # Equivalent to [ if (yp[i] == test_labels[i]): correct_examples += 1 ]
    correct_examples = abs(sum(1 - (abs(yp - test_labels) / 2)))
    
    # Calculate test accuracy
    test_acc = 100*correct_examples / total_examples
    return test_acc, correct_examples, total_examples

# Write epoch-specific data to local log, and total data to master_log
def write_to_log(h_vars, cost_array, test_acc_array, sample_array, train_time):
    ### Logging Variables ###
    log_name                = "log_" + str(datetime.now().strftime("%d_%m_%Y_%H_%M_%S") + ".csv")
    path_to_master_log      = path_to_log_folder + "_master_log.csv"
    path_to_log             = path_to_log_folder + log_name

    ### Logs Folder ###
    # Create "/logs" folder if it does not exist
    if (not os.path.exists(path_to_log_folder)):
        os.mkdir(path_to_log_folder)

    ### Master Log ###
    # Create "_master_log.csv" if it does not exist
    first_log = (not os.path.exists(path_to_master_log))
    g = open(path_to_master_log, "a+")
    if (first_log):
        g.write(",cluster_count,cores_per_cluster,test_acc,train_time,examples_until_pass_token,total_epochs,learning_rate,regularization_strength,initial_step_size,initial_step_decay\n")
    
    # Write this run's data into master log
    g.write(str(log_name) + "," + str(h_vars['cluster_count']) + "," + str(h_vars['cores_per_cluster']) + "," + str(test_acc_array[-1]) + "," + str(train_time) + \
            str(h_vars['examples_until_pass_token']) + "," + str(h_vars['total_epochs']) + "," + str(h_vars['learning_rate']) + "," + \
            str(h_vars['regularization_strength']) + "," + str(h_vars['initial_step_size']) + "," + str(h_vars['initial_step_decay']) + "\n")
    g.close()

    ### Local Log for this run ###
    f = open(path_to_log, "w+")

    # Write header
    f.write("record,sample,test_acc,lowest_cost\n")

    # Write data from each epoch of this run
    for i in range(len(sample_array)):
        f.write(str(i) + "," + str(sample_array[i]) + "," + str(test_acc_array[i]) + "," + str(cost_array[i]) + "\n")
    f.close()

if __name__ == '__main__':
    driver()