import numpy as np
from scipy.sparse import csr_matrix

#  >> MODEL TRAINING << #
# compute_cost(weights, input, y_i)
# - Compute the cost of the SVM according to hinge loss cost function
# - Note that the W includes b at the end, and X includes 1's at the end
#   so that the dot product includes the bias term
def compute_cost(global_dict, W, X, Y):

    # Get dimension
    N = len(W)

    # 1 - y(x.w + b)
    distances = 1 - Y * (X*np.array(W).transpose())

    # Equivalent to max(0, distance)
    distances[distances < 0] = 0  

    # C * [(1/N)sum(max(0, 1 - y(x.w + b))]
    hinge_loss = global_dict['regularization_strength'] * (np.sum(distances) / N)

    # Calculate total cost
    cost = 1 / 2 * np.dot(W, W) + hinge_loss
    return cost

# calculate_cost_gradient - returns dw
def calculate_cost_gradient(global_dict, _W, _X, Y):
    # If only one example is passed (eg. in case of SGD)
    # convert to an array
    W = np.array(_W)
    X = _X.reshape((1, len(W)))

    dw = np.zeros((1, len(W)))

    distance = 1 - (Y * (X*W.transpose()))

    # For each element in distance ( distance[ind] = d ):
    for ind, d in enumerate(distance):

        # If max(0, d) == 0, then W
        if max(0, d) == 0:
            dw = W

        # Else, W - Cy[i]x[i]
        else:
            di = W - (global_dict['regularization_strength'] * Y * X)
            dw += di
    dw = dw.reshape((1, len(W)))
    #print("dw: " + str(dw))
    return dw
