# accuracy_verifier.py
# Verify the accuracy of a dataset using a model vector
# Datasets   : covtype  - 500000 train, 81012 test, 54 features       
#            : news20   - 15000 train, 4996 test, 1355191 features    
#            : epsilon  - 400000 train, 100000 test, 2000 features    
#            : rcv1     - 20242 train, 677399 test, 47236 features    
#            : webspam  - 300000 train, 50000 test, 250 features 
#            : a1a      - 1605 train, 30956 test, 123 features
#            : german   - 800 train, 200 test, 24 features
#            : smallboi - 6 train, 0 test, 5 features

import numpy as np
from numpy import genfromtxt
import pandas as pd
from scipy.sparse import csr_matrix

### Globals (SET THESE) ###
model_vector_csv_path  = './data/covtype_model_vector.csv'
test_features_csv_path = './data/covtype_test_features.csv'
test_labels_csv_path   = './data/covtype_test_labels.csv'
feature_count          = 54     # Note, this does NOT include bias.


def driver():
    # Prepare data
    test_features, test_labels = prepare_data()
    
    # Get model vector
    W = genfromtxt(model_vector_csv_path, delimiter=',')

    # Do testing
    test_acc, correct_examples, total_examples = test(test_features, test_labels, W)

    # Print out results!
    print("Test Accuracy: " + str(test_acc) + "% (" + str(int(correct_examples)) + " / " + str(total_examples) + ")")

# Retrieve test accuracy for a given <feature vector> and <test samples>
def test(test_features, test_labels, W):
    correct_examples, total_examples = 0., test_features.shape[0]

    # classification = sign( X . W )
    yp = np.sign(test_features * np.array(W).transpose())

    # Equivalent to [ if (yp[i] == test_labels[i]): correct_examples += 1 ]
    correct_examples = abs(sum(1 - (abs(yp - test_labels) / 2)))

    # Calculate test accuracy
    test_acc = 100*correct_examples / total_examples
    return test_acc, correct_examples, total_examples

# Return the arrays of test_features and test_labels
def prepare_data():
    print("Reading testing data...")
    test_features_df = pd.read_csv(test_features_csv_path)
    test_labels_df   = pd.read_csv(test_labels_csv_path)
    
    print("Constructing matrices... ")
    test_features  = get_dataframe(test_features_df, 'feature')
    test_labels    = get_dataframe(test_labels_df, 'label')
    print("Constructed testing data  " + str(test_features.shape))
    return test_features, test_labels

# Return df from file
def get_dataframe(data, file_type):
    # If features, put into a csr_matrix
    if file_type == 'feature':
        
        row_count = data['row_count']
        # If -1, read all samples!
        total_samples = row_count.iloc[-1] + 1
        row_count = np.array(data['row_count'])
        index     = np.array(data['index'])
        value     = np.array(data['value'])

        # Add intercept
        extended_row_count = np.array([i for i in range(total_samples)])
        extended_index     = np.array([feature_count for i in range(total_samples)])
        extended_value     = np.array([1 for i in range(total_samples)])
        row_count = np.concatenate((row_count, extended_row_count))
        index     = np.concatenate((index, extended_index))
        value     = np.concatenate((value, extended_value))

        # Create csr_matrix
        return csr_matrix((value, (row_count, index)), shape=(total_samples, feature_count + 1))

    elif file_type == 'label':
        return np.array(data['label'])

    elif file_type == 'vector':
        return np.array(data)

if __name__ == '__main__':
    driver()